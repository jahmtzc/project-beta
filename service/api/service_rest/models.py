from django.db import models

# Create your models here.
class Technician(models.Model):
    first_name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    employee_id=models.CharField(max_length=200)

class Appointment(models.Model):


    date_time=models.DateTimeField()
    reason=models.CharField(max_length=20000)
    vin=models.CharField(max_length=200)
    customer=models.CharField(max_length=200, null=True)

    status=models.CharField(max_length=200)
    technician=models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT,
    )



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
