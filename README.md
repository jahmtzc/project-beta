# CarCar

CarCar is an application that allows a car dealership to manage inventory, sales, and car services.

Team:

* **Fred Bowden** - Sales
* **Jahaziel Martinez** - Services

## How to Run this App

**Have your terminal, Docker, and Insomnia ready

1. Fork the repository

2. Select the Clone with HTTPS option

3. Open a folder in your terminal where the repo can live

4. Type: git clone <<https_url>>

5. cd into the newly cloned repository

6. Setup docker(create volumes for the data, build images, build containers):
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- Monitor your Docker desktop until your containers are all up and running
- Type in your browser: http://localhost:3000/
- You should now see the main page of the application with a navigation at the top of the page

## Diagram

![Img](/diagrams/J%26F%20CarCar%20Diagram.png)

## API Documentation

### URLs and Ports

![Img](/images/Sales%20Microservice%20URLs.png)
![Img](/images/service-microservice-urls.png)
### Sales API

The sales microservice consists of the four models found in sales/sales_rest/models.py: Salesperson, Customer, AutomobileVO, and Sale. The AutomobileVO is polled from the inventory api while the docker container is running. This allows every car in the inventory to have the ability to be sold.

## Using http methods(GET, POST, DELETE) to access or interact with api endpoints

Open Insomnia and create a folder titled "Sales Microservice"

Use the URLs provided in the Sales Microservice png file above

Every endpoint will provide a json message if it doesn not exist

**Salesperson endpoints**
POST a salesperson(send this json body):
Cannot post a salseperson with the same employee_id
```
{
    "first_name": "Fred",
    "last_name": "Bowden",
    "employee_id": "123456"
}
```
Return value:
```
{
	"salesperson": {
		"first_name": "Fred",
		"last_name": "Bowden",
		"employee_id": "123456",
		"id": 1
	}
}
```

GET a list of salespeople return value:
```
{
	"salespeople": [
		{
			"first_name": "Fred",
			"last_name": "Bowden",
			"employee_id": "123456",
			"id": 1
		},
		{
			"first_name": "Jahaziel",
			"last_name": "Martinez",
			"employee_id": "654321",
			"id": 2
		}
	]
}
```
DELETE a salesperson return value(once deleted value becomes false):
```
{
	"delete": true
}
```
**Customer endpoints**
POST a customer(send this json body):
```
{
    "first_name": "Jacky",
    "last_name": "Li",
    "address": "123 sesame street",
    "phone_number": "0123456789"
}
```
Return value:
```
{
	"customer": {
		"first_name": "Jacky",
		"last_name": "Li",
		"address": "123 sesame street",
		"phone_number": "0123456789",
		"id": 3
	}
}
```
GET a list of customers return value:
```
{
	"customers": [
		{
			"first_name": "Russ",
			"last_name": "Cruz",
			"address": "123 sesame street",
			"phone_number": "0123456789",
			"id": 1
		},
		{
			"first_name": "Max",
			"last_name": "Wang",
			"address": "123 sesame street",
			"phone_number": "0123456789",
			"id": 2
		},
		{
			"first_name": "Jacky",
			"last_name": "Li",
			"address": "123 sesame street",
			"phone_number": "0123456789",
			"id": 3
		}
	]
}
```
DELETE a customer return value(once deleted value becomes false):
```
{
	"delete": true
}
```
**Sales endpoints**
POST a sale(send this json body):
Cannot post a sale with an invalid salesperson(employe_id property), customer(first_name property), or automobile(vin property)
```
{
    "price": "50,000",
    "customer": "Russ",
    "salesperson": "123456",
    "automobile": "55555"
}
```
Return value:
The values of the customer, salesperson, and automobile are converted to the json object of the instance that was chosen
The sold value of the automobile is updated from its default value of false to now equal true
```
{
	"sale": {
		"id": 1,
		"price": "50,000",
		"customer": {
			"first_name": "Russ",
			"last_name": "Cruz",
			"address": "123 sesame street",
			"phone_number": "0123456789",
			"id": 1
		},
		"salesperson": {
			"first_name": "Jahaziel",
			"last_name": "Martinez",
			"employee_id": "654321",
			"id": 2
		},
		"automobile": {
			"vin": "22222",
			"sold": true
		}
	}
}
```
GET a list of sales return value:
```
{
	"sales": [
		{
			"id": 1,
			"price": "30,000",
			"customer": {
				"first_name": "Max",
				"last_name": "Wang",
				"address": "123 sesame street",
				"phone_number": "0123456789",
				"id": 2
			},
			"salesperson": {
				"first_name": "Jahaziel",
				"last_name": "Martinez",
				"employee_id": "654321",
				"id": 2
			},
			"automobile": {
				"vin": "22222",
				"sold": true
			}
		},
        {
            "id": 2,
            "price": "35,000",
            "customer": {
                "first_name": "Jacky",
                "last_name": "Li",
                "address": "123 sesame street",
                "phone_number": "0123456789",
                "id": 3
            },
            "salesperson": {
                "first_name": "Jahaziel",
                "last_name": "Martinez",
                "employee_id": "654321",
                "id": 2
            },
            "automobile": {
                "vin": "44444",
                "sold": true
            }
        }
    ]
}
```
DELETE a sale return value(once deleted value becomes false):
```
{
	"delete": true
}
```
### Services API
The Services Microservice consists of three models: Technician, Appointment, and AutomobileVO. The AutomobileVO is polled from the inventory api while the docker container is running. The Appointment has a Foreign Key to the Technician model so that appointments can only be created using existing Technician objects. The AutomobileVO could have been used to implement the VIP special feature; however, I chose to do a front end implementation of the feature directly pulling data from the inventory-api.

## Using http methods(GET, POST, DELETE) to access or interact with api endpoints

Open Insomnia and create a folder titled "Service Microservice"

Use the URLs provided in the "service-microservice-urls" file above



**Technician endpoints**
POST Request: http://localhost:8080/api/technicians/ (BODY TYPE: JSON):

```
{
	"first_name": "jahaziel",
	"last_name": "martinez",
	"employee_id": "12345"
}
```
Return value:
```
{
	"first_name": "jahaziel",
	"last_name": "martinez",
	"employee_id": "12345",
	"id": 1
}
```

GET a list of Techncians return value:
```
{
	"technicians": [
		{
			"first_name": "jahaziel",
			"last_name": "martinez",
			"employee_id": "12345",
			"id": 1
		},
		{
			"first_name": "Fred",
			"last_name": "Bowden",
			"employee_id": "54321",
			"id": 2
		}
	]
}
```
DELETE a salesperson return value(once deleted value becomes false):
```
{
	"first_name": "Fred",
	"last_name": "Bowden",
	"employee_id": "54321",
	"id": null
}

The null value in ID field means it no longer exists as an instance.
```
**Appointment endpoints**
POST: http://localhost:8080/api/appointments/ (BODY: JSON):
```
{
	"vin": "12345",
	"date_time": "2023-06-25 14:00:00",
	"customer": "Jay",
	"technician": "1",
	"reason": "Needs oil change",
	"status": "created"
}
```
Return value:
```
{
	"id": 1,
	"date_time": "2023-06-25 14:00:00",
	"reason": "Needs oil change",
	"vin": "12345",
	"technician": {
		"first_name": "jahaziel",
		"last_name": "martinez",
		"employee_id": "12345",
		"id": 1
	},
	"customer": "Jay",
	"status": "created"
}
```
GET a list of Appointments return value:
```
{
	"appointments": [
		{
			"id": 2,
			"date_time": "2023-06-09T12:53:00+00:00",
			"reason": "Fuel Pump Lock Ring",
			"vin": "FM54321",
			"technician": {
				"first_name": "Cristian",
				"last_name": "Cano",
				"employee_id": "1",
				"id": 1
			},
			"customer": "Susie",
			"status": "finished"
		},
		{
			"id": 1,
			"date_time": "2023-06-25T12:52:00+00:00",
			"reason": "Oil Change",
			"vin": "FM12345",
			"technician": {
				"first_name": "Cristian",
				"last_name": "Cano",
				"employee_id": "1",
				"id": 1
			},
			"customer": "Miguel",
			"status": "canceled"
		},
		{
			"id": 3,
			"date_time": "2023-06-27T16:00:00+00:00",
			"reason": "fuel pump",
			"vin": "HC12345",
			"technician": {
				"first_name": "Cristian",
				"last_name": "Cano",
				"employee_id": "1",
				"id": 1
			},
			"customer": "Gordo",
			"status": "finished"
		}]}
```
DELETE a Appointment return value(The id becomes null once deleted. The status remains at the value which it was at the time of deletion(created, finished, canceled. This is not to be confused with canceling an appointment, this will just remove the instance from the database.)):
```
{
	"id": null,
	"date_time": "2023-06-25T00:00:00+00:00",
	"reason": "Needs Brake replacement",
	"vin": "1C3CC5FB2AN120174",
	"technician": {
		"first_name": "fri",
		"last_name": "martinez",
		"employee_id": "12345",
		"id": 1
	},
	"status": "created"
}
```
PUT REQUESTS:
FINISH: PUT REQUEST TO: http://localhost:8080/api/appointments/2/finish/
This changes the Status field to "finished". You do not need to include a body.
CANCEL: PUT REQUEST TO: http://localhost:8080/api/appointments/3/cancel/
This changes the status field to "canceled". You do not need to include a body.

## Value Objects
**Sales microservice**
- The AutomobileVO model class is a value object that is created from poller data from the inventory api, where the automobile instances are stored. The AutomobileVO is used for the creation of a Sale. Once sold on the application the automobile instance sold property is set to true which disqaulifies the car from being available for sale though it is still saved in the database.

**Services microservice**
- The AutomobileVO model class is a value object that is created from poller data from the inventory api, where the automobile instances are stored. This could've been used as a means to implement the VIP feature, however, as I explained earlier, I implemented that feature in the front end instead. Although the Technician object is considered an entity because it can be edited etc, I believe in its use in the Appointment functions it is considered a Value Object since ultimately only an unique and created Technician can be assigned to a specific appointment.
