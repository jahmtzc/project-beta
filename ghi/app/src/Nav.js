import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/"><img src="https://i.imgur.com/Ge5wUDat.png" alt="Nate's Mobile Logo"></img></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/automobiles/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Automobiles
            </a>
              <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/automobiles/">All Automobiles</a></li>
                <li><a className="dropdown-item" href="/automobiles/create/">Add an Automobile</a></li>
                <li><a className="dropdown-item" href="/manufacturers/">Makes</a></li>
                <li><a className="dropdown-item" href="/models/">Models</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/sales/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </a>
              <ul className="dropdown-menu">
                <li><a className="dropdown-item" href="/sales/">All Sales</a></li>
                <li><a className="dropdown-item" href="/sales/create/">Add a Sale</a></li>
                <li><a className="dropdown-item" href="/salespeople/">Salespeople</a></li>
                <li><a className="dropdown-item" href="/salespeople/create/">Add Salesperson</a></li>
                <li><a className="dropdown-item" href="/customers/">Customers</a></li>
                <li><a className="dropdown-item" href="/customers/create/">Add a Customer</a></li>
                <li><a className="dropdown-item" href="/sales/history/">Salesperson History</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="/appointments/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Service
            </a>
              <ul className="dropdown-menu">
                <li><a className="dropdown-item" href="/appointments/">Appointments</a></li>
                <li><a className="dropdown-item" href="/appointments/create/">Add an Appointment</a></li>
                <li><a className="dropdown-item" href="/technicians/">Technicians</a></li>
                <li><a className="dropdown-item" href="/technicians/create/">Add a Technician</a></li>
                <li><a className="dropdown-item" href="/appointments/history/">Appointment History</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
