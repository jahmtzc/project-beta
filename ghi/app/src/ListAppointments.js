import { useEffect, useState } from "react";

function ListAppointments() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const fetchAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles');
    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
    }  else {
        console.error(response);
      }
    }
    const fetchAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      const createdAppts = data.appointments.filter(appt => appt.status==="created")
      setAppointments(createdAppts);
  } else {
    console.error(response);
  }}
  useEffect(() => {
    fetchAppointments();
    fetchAutos();
  }, []);


  const handleFinish = async (id) => {
    const statusUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
    const fetchOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const finishResponse = await fetch(statusUrl, fetchOptions);
    if (finishResponse.ok) {
      console.log("message: successfully marked as finished.")
      fetchAppointments();
    }}

    const handleCancel = async (id) => {
      const statusUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
      const fetchOptions = {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const finishResponse = await fetch(statusUrl, fetchOptions);
      if (finishResponse.ok) {
        console.log("message: successfully marked as canceled.")
        fetchAppointments();
      }}

function VIP(vin, autoslist) {
    let vinlist = [];

    for (const auto of autoslist){
        vinlist.push(auto.vin)}
    if (vinlist.includes(vin)){
        return <td>Yes</td>;
    } else {
        return <td>No</td>;
    }}


  return(
    <table className="table table-striped">
        <thead>
            <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th> </th>
            <th> </th>
            </tr>
        </thead>
        <tbody>
            {appointments.map(appt => {
              const newDate = new Date(appt.date_time)
              const options = {
                day: "numeric",
                month: "numeric",
                year: "numeric",
              };
            return(<tr key={appt.id}>
                <td>{appt.vin}</td>
                    {VIP(appt.vin, autos)}
                <td>{appt.customer}</td>
                <td>{newDate.toLocaleString('en-US', options)}</td>
                <td>{newDate.toLocaleTimeString('en-US')}</td>
                <td>{appt.technician.first_name} {appt.technician.last_name}</td>
                <td>{appt.reason}</td>
                <td><button name="finishButton" onClick={() => handleFinish(`${appt.id}`)}>Finish</button></td>
                <td><button name="cancelButton" onClick={() => handleCancel(`${appt.id}`)}>Cancel</button></td>
            </tr>
            )})}
        </tbody>

</table>
  )
            }

export default ListAppointments;
