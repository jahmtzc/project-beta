function MainPage(props) {
  console.log(props.inventory)
  return (
    <div className="px-4 py-5 my-5 text-center">
      <img src="https://i.pinimg.com/564x/87/1c/5a/871c5a9a19cf76f6b3e9e1e0140bee41.jpg"></img>
      <h1 className="display-5 fw-bold">Welcome to Nate's Mobile!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Bringing all your car solutions to your doorstep.
        </p>
      </div>
    </div>
  );
}

export default MainPage;
