from django.urls import path
from .views import list_sales_people, show_sales_person_detail, list_customers, show_customer_detail, list_sales, show_sale_detail

urlpatterns = [
    path('salespeople/', list_sales_people, name="list_sales_people"),
    path('salespeople/<int:id>', show_sales_person_detail, name="show_sales_person_detail"),
    path('customers/', list_customers, name='list_customers'),
    path('customers/<int:id>/', show_customer_detail, name="show_customer_detail"),
    path('sales/', list_sales, name="list_sales"),
    path('sales/<int:id>/', show_sale_detail, name="show_sale_detail")
]
