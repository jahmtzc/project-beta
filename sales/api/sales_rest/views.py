from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO

class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ['first_name', 'last_name', 'employee_id', 'id']

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['first_name','last_name','address','phone_number', 'id']

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin','sold']

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ['id','price','customer','salesperson','automobile']
    encoders={'customer': CustomerEncoder(), 'salesperson': SalesPersonEncoder(), 'automobile': AutomobileVOEncoder()}

@require_http_methods(['GET','POST'])
def list_sales_people(request):
    if request.method == 'GET':
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse({"salespeople": salespeople}, encoder=SalesPersonEncoder)
        except:
            return JsonResponse({"message": "Could not get salespeople."}, status=400)
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse({"salesperson": salesperson}, encoder=SalesPersonEncoder)
        except:
            return JsonResponse({"message": "Could not create a salesperson."}, status=400)

@require_http_methods(['GET','DELETE'])
def show_sales_person_detail(request, id):
    if request.method == 'GET':
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse({"salesperson": salesperson}, encoder=SalesPersonEncoder)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist."}, status=400)
    else:
        try:
            count,_ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({"delete": count > 0})
        except:
            return JsonResponse({"message": "Could not delete a salesperson."}, status=400)

@require_http_methods(['GET', 'POST'])
def list_customers(request):
    if request.method == 'GET':
        try:
            customers = Customer.objects.all()
            return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
        except:
            return JsonResponse({"message": "Could not get the customers."}, status=400)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
        except:
            return JsonResponse({"message": "Could not create a customer."}, status=400)

@require_http_methods(['GET','DELETE'])
def show_customer_detail(request, id):
    if request.method == 'GET':
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist."}, status=400)
    else:
        try:
            count,_ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"delete": count > 0})
        except:
            return JsonResponse({"message": "Could not delete a customer."}, status=400)

@require_http_methods(['GET','POST'])
def list_sales(request):
    if request.method == 'GET':
        try:
            sales = Sale.objects.all()
            return JsonResponse({"sales": sales}, encoder=SaleEncoder)
        except:
            return JsonResponse({"message": "Could not get the sales."}, status=400)
    else:
        content = json.loads(request.body)
        try:
            content_vin = content['automobile'] #GET THE CONTENT BODY
            automobile = AutomobileVO.objects.get(vin=content_vin) #TRY TO GET THE INSTANCE
            if automobile.sold: #CHECK IF IT IS SOLD ALREADY
                return JsonResponse({"message": "Automobile is already sold."}, status=400)
            content['automobile'] = automobile #SET THE CONTENT TO THE INSTANCE
            automobile.sold = True
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile does not exist."}, status=400)
        try:
            content_id = content['salesperson']
            salesperson = Salesperson.objects.get(employee_id=content_id)
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist."}, status=400)
        try:
            content_name = content['customer']
            customer = Customer.objects.get(first_name=content_name)
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist."}, status=400)

        sale = Sale.objects.create(**content)
        return JsonResponse({"sale": sale}, encoder=SaleEncoder)

@require_http_methods(["GET","DELETE"])
def show_sale_detail(request, id):
    if request.method == 'GET':
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse({"sale": sale}, encoder=SaleEncoder)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist."}, status=400)
    else:
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"delete": count > 0})
        except:
            return JsonResponse({"message": "Could not delete a sale."}, status=400)
